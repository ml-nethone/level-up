# -*- coding: utf-8 -*
from sklearn.preprocessing import FunctionTransformer

from sklearn.pipeline import Pipeline
from transformers import *

categorical_columns = [
    'user_ip_country',
    'bin_country',
    'card_type',
    'card_brand',
    'headers_connection',
    'navigatorappname',
    'navigatorproduct',
    'navigatorappcodename',
    'donottrack'    
]

dict_columns = [
    'screen',
    'flash_fields',
    'dwell_time',
    'flight_time',
]

## pomocnicze subpipeliny - wyciągają listy z podanych kolumn

headers_encoding_pipe = Pipeline([
    ('Select column', ColumnSelector(['headers_accept_encoding'])),
    ('Extract encodings', ExtractRegex('[a-z]+'))
])

flash_fonts_pipe = Pipeline([
    ('Select column', ColumnSelector(['flash_fonts'])),
    ('Convert to lists', FunctionTransformer(lambda x: x.applymap(as_list), validate=False))
])

navigatorlanguages_pipe = Pipeline([
    ('Select column', ColumnSelector(['navigatorlanguages'])),
    ('Convert to lists', FunctionTransformer(lambda x: x.applymap(as_list), validate=False))
])

headers_accept_language_pipe = Pipeline([
    ('Select column', ColumnSelector(['headers_accept_language'])),
    ('Extract languages', ExtractRegex('[a-z]{2}-?[A-Za-z]{0,2}'))
])

extract_browsers_pipe = Pipeline([
    ('Select column', ColumnSelector(['headers_user_agent'])),
    ('Extract languages', ExtractRegex('([A-Za-z]+)/[0-9.]+ '))
])

## główne pipeliny

# długości list
list_length_pipeline = Pipeline([
    ('Select and transform list columns', DaftFeatureUnion([
        ('headers_accept_encoding', headers_encoding_pipe),
        ('flash_fonts', flash_fonts_pipe),
        ('navigatorlanguages', navigatorlanguages_pipe),
        ('headers_accept_language', headers_accept_language_pipe),
        ('browsers', extract_browsers_pipe)
    ])),
    ("List length", ListLengthTransformer()),
    ("Add suffix", AddColumnSuffix('list_length'))
])

# podobieństwo
language_similarity_pipeline = Pipeline([
    ('Select and transform list columns', DaftFeatureUnion([
        ('navigatorlanguages', navigatorlanguages_pipe),
        ('headers_accept_language', headers_accept_language_pipe),        
    ])),
    ("Calculate similarity", PairwiseColumnOperations(set_similarity_pairwise,
                                            fn_name='set_similarity',
                                            colname_pair_list=[['navigatorlanguages', 'headers_accept_language']]
                                            )),
    ("Add suffix", AddColumnSuffix('similarity'))
])

# ilości języków
language_count_pipeline = Pipeline([
    ('Select and transform list columns', DaftFeatureUnion([
        ('navigatorlanguages', navigatorlanguages_pipe),
        ('headers_accept_language', headers_accept_language_pipe),
    ])),
    ('Number of languages', NumberOfLanguages(columns=[
        'navigatorlanguages',
        'headers_accept_language'
    ])),
    ("Add suffix", AddColumnSuffix('language_count_pipeline'))
])


## Wersje przeglądarek
browser_version_pipeline = Pipeline([
    ('Select column', ColumnSelector(['headers_user_agent'])),
    ('Get browser versions', DaftFeatureUnion([
        ('Chrome major version', ExtractRegex('Chrome/([0-9]+)', first_only=True, regex_name='chrome_version')),
        ('Mozilla major version', ExtractRegex('Mozilla/([0-9]+)', first_only=True, regex_name='mozilla_version')),
        ('Safari major version', ExtractRegex('Safari/([0-9]+)', first_only=True, regex_name='safari_version')),
    ])),    
    ('To integer', FunctionTransformer(lambda x: x.apply(pd.to_numeric), validate=False))
    
])

### pozostałe

# kategoryki - tylko przekazanie dalej z suffixem
categorical_pipeline = Pipeline([
    ('Select Columns', ColumnSelector(col_list=categorical_columns)),
    #('Encode', HashingTrick(max_cols=200)),
    ("Add suffix", AddColumnSuffix('categorical'))
])

# kategoryki - zakodowane hashing trickiem
categorical_pipeline_hashed = Pipeline([
    ('Select Columns', ColumnSelector(col_list=categorical_columns)),
    ('Encode', HashingTrick(max_cols=10)),
    ("Add suffix", AddColumnSuffix('categorical_HT'))
])

# numeryki - przekazanie dalej z suffixem. Ważne - wywalamy id
numerical_pipeline = Pipeline([
    ('Select Columns', ColumnSelector(types_list=[pd.np.number])),
    ('Drop ids', DropColumn(['issuer_id'])),
    ("Add suffix", AddColumnSuffix('numerical'))
])

# rozpakowanie kolumn ze słownikami
dict_unpack_pipeline = Pipeline([
    ('Select Columns', ColumnSelector(col_list=dict_columns)),
     ('Unpack dicts', DictUnpacker()),
     ('Process lists', ListUnpacker()),
     ('FillColumns', ColumnFiller()),
     ("Add suffix", AddColumnSuffix('dict_unpack_pipeline'))
])

# wielkość słownika
dict_volume_pipeline = Pipeline([
    ('Select Columns', ColumnSelector(col_list=dict_columns)),
     ('Dict volume', DictVolume(columns=dict_columns)),
     ("Add suffix", AddColumnSuffix('dict_volume_pipeline'))
])

# relacja między czasami
time_relation = Pipeline([
    ('Select Columns', ColumnSelector(col_list=['timeofrequestresponse','timeoftotalpageload'])),
    ('rel', ColumnsRelation(columns=['timeofrequestresponse','timeoftotalpageload'], operator='/')),
     ("Add suffix", AddColumnSuffix('time_relation_pipeline'))
])

# wyciągnięcie informacji o systemie z kolumny z rozpakowanego słownika
extended_dict_pipeline = Pipeline([
    ('Dict', dict_unpack_pipeline),
    ('Additional features', DaftFeatureUnion([
        ('Pass previous features', DummyTransformer()),
        ('Add system info', Pipeline([
            ('Select column', ColumnSelector(col_list=['flash_fields_Capabilities.os_dict_unpack_pipeline'])),
            ('Extract system', ExtractRegex('[A-Za-z 0-9]+', first_only=True, regex_name='system_version'))
        ]))
    ])),
])

# sprawdzenie czy kraje się zgadzają
column_equals_pipeline = Pipeline([
    ('Select Columns', ColumnSelector(col_list=['bin_country','user_ip_country'])),
    ('CheckIfEquals', TwoColumnsEquals([('bin_country','user_ip_country')])),
    ("Add suffix", AddColumnSuffix('column_equals_pipeline'))
])

# połączony pipeline
feature_extraction_subpipeline = DaftFeatureUnion([
    ('Categorical', categorical_pipeline),
    ('Numerical', numerical_pipeline),
    ('Dict', extended_dict_pipeline),
    ('Dict vol', dict_volume_pipeline),
    ('Column equals', column_equals_pipeline),
    ('List length', list_length_pipeline),
    ('language_count_pipeline', language_count_pipeline),
    ('Language intersection', language_similarity_pipeline),
    ('Browser versions', browser_version_pipeline),
    ('Time relation', time_relation)
])

# pipeline tylko na kolumnach fingerprintowych
fingerprint_pipeline = DaftFeatureUnion([
    ('Dict', dict_unpack_pipeline),
    ('List length', list_length_pipeline),
    ('Language intersection', language_similarity_pipeline),
    ('Browser versions', browser_version_pipeline),
    ('language_count_pipeline', language_count_pipeline),
    ('Dict vol', dict_volume_pipeline),
    ('Time relation', time_relation)
])

# ostateczny pipeline
final_pipeline = Pipeline([
    ('All features', feature_extraction_subpipeline),
    ('Fill nan', FillNA(-1)),
    ('Hash', SimpleHash())
])
