# -*- coding: utf-8 -*
import pandas as pd
import numpy as np

from sklearn.pipeline import _transform_one, _fit_transform_one
from sklearn.externals.joblib import Parallel, delayed
from sklearn.pipeline import FeatureUnion
from sklearn.base import TransformerMixin
from sklearn.utils import murmurhash3_32
import re
import ast

import json
from collections import Counter


def split_train_test(X, y, cutoff=.66):
    split = int(X.shape[0] * cutoff)
    return X.iloc[:split, :], y[:split], X.iloc[split:, :], y[split:]


class DaftFeatureUnion(FeatureUnion):
    """
    Łączy wyjście z podanych pipelinów w jedną ramkę zachowując nazwy kolumn.
    """
    def fit_transform(self, X, y=None, **fit_params):
        result = Parallel(n_jobs=self.n_jobs)(
            delayed(_fit_transform_one)(trans, weight, X, y,
                                        **fit_params)
            for name, trans, weight in self._iter())

        Xs, transformers = zip(*result)
        self._update_transformer_list(transformers)
        return pd.concat(Xs, axis=1)

    def transform(self, X, **kwargs):
        Xs = Parallel(n_jobs=self.n_jobs)(
            delayed(_transform_one)(trans, weight, X, **kwargs)
            for name, trans, weight in self._iter())

        return pd.concat(Xs, axis=1)


class HashingTrick(TransformerMixin):
    """
    Koduje zmienne kategoryczne na wektor binarny. Pozwala na ograniczenie
    ilości otrzymanych kolumn.
    """
    def __init__(self, max_cols=1000):
        self._max_cols = max_cols
        
    def fit(self, X, y=None, **fit_params):
        return self
    
    def transform(self, X, **kwargs):
        max_cols = self._max_cols
        nrows = X.shape[0]
        new_X = np.zeros((nrows, max_cols))
        for i in range(X.shape[0]):
            line = X.iloc[i]
            column_label = [str(k) + str(v) for k, v in dict(line).items()]
            hashed = list(map(lambda v: murmurhash3_32(v) % max_cols, column_label))
            assert pd.notnull(hashed).any()
            encoded = np.zeros(max_cols)
            for h in hashed:
                encoded[h] = 1.0
            new_X[i, :] = encoded
        new_X = pd.DataFrame(new_X, columns=range(max_cols), index=X.index)
        return new_X


class CounterTransformer(TransformerMixin):
    """
    Zastępuje wartość kategoryczną licznością jej wystąpień w zbiorze treningowym.
    Dla nowych obserwacji zwraca 0.
    Uwaga - X musi być obiektem typu Series!
    """
    def __init__(self):
        self._counts = None
        
    def fit(self, X, y=None, **fit_params):
        self._counts = X.value_counts() / 1.0 / X.shape[0]
        return self
    
    def transform(self, X, **kwargs):
        return X.apply(lambda v: self._counts.get(v, 0))


class DFCounterTransformer(TransformerMixin):
    """
    CounterTransformer zastosowany dla każdej kolumny w danych.
    """
    def __init__(self):
        self._transformers = {}
    
    def fit(self, X, y=None, **fit_params):
        self._transformers = {col: CounterTransformer().fit(X[col]) for col in X.columns}
        return self
    
    def transform(self, X, **kwargs):
        return pd.concat([
            self._transformers[col].transform(X[col]) for col in X.columns
        ], axis=1)


class SimpleHash(TransformerMixin):
    """
    Dokonuje hashowania każdej wartości nieliczbowej w ramce danych.
    """
    def __init__(self, mod=100000):
        self.mod = mod
    
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        return (
            X.applymap(
                lambda x: murmurhash3_32(x) % self.mod
                if not isinstance(x, (int, float))
                else x
            ).apply(pd.to_numeric, axis=1))


class ColumnSelector(TransformerMixin):
    """
    Zwraca ramkę o kolumnach podanych według nazwy lub według typów.
    """
    def __init__(self, col_list=None, types_list=None):
        self.col_list = col_list
        self.types_list = types_list
        assert (col_list is None) + (types_list is None) == 1
        
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        if self.col_list:
            return X[self.col_list]
        else:
            return X.select_dtypes(self.types_list)


class DropColumn(TransformerMixin):
    """
    Wyrzuca z ramki kolumny podane jako lista nazw.
    """
    def __init__(self, cols_to_drop):
        self.cols_to_drop=cols_to_drop
    
    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, y=None, **kwargs):
        return X.drop(self.cols_to_drop,axis=1)
        

class FillNA(TransformerMixin):
    """
    Uzupełnia dane stałą wartością.
    """
    def __init__(self, fill=-1):
        self.fill = fill
        
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        return X.applymap(lambda x: x if np.all(pd.notnull(x)) else self.fill)


def jsonstr2dict(val):
    """
    Funkcja pomocnicza do DictUnpackera. Konwertuje słownik zapisany jako tekst na słownik pythonowy.
    """
    if (
        isinstance(val, str)
    ):
        try:
            return json.loads(val)
        except:
            return val
    else:
        return val


def recursive_dict_to_columns(Y):
    """
    Zamienia w ramce danych kolumny ze słownikami na podramki z rozwiniętymi kolumnami.
    """
    if Y is not None:
        for col in Y.columns:
            is_dict = any(
                Y[col].apply(lambda x: isinstance(x, dict))
            )
            if is_dict:
                dict_vals = [
                    x if pd.notnull(x) else dict()
                    for x in Y[col]
                ]
                Y_undict_cols = pd.DataFrame.from_dict(dict_vals)
                Y_undict_cols = Y_undict_cols.dropna(axis=1, how='all')
                Y_undict_cols = Y_undict_cols.add_prefix(col + '_')
                Y = pd.concat([Y, Y_undict_cols], axis=1)
                Y.drop(col, axis=1, inplace=True)
                recursive_dict_to_columns(Y)
            else:
                continue
    return Y


class DictUnpacker(TransformerMixin):
    """
    'Rozpakowuje' kolumny ze słownikami.
    Przykład: ramka o jednej kolumnie 'x' i jednej obserwacji równej {'a': 5, 'b': {'c': 10}}
    zostanie rozwinięta do ramki o kolumnach 'x_a' i 'x_b_c', z wartościami 5 i 10.
    """
    def __init__(self):
        self.dict_columns = None

    def fit(self, X, y=None, **kwargs):
        self.dict_columns = []
        for col in X.columns:
            processed_json = X[col].apply(jsonstr2dict)
            is_dict = any(
                processed_json.apply(lambda x: isinstance(x, dict))
            )
            if is_dict:
                self.dict_columns.append(col)
        return self

    def transform(self, X, y=None, **kwargs):
        result = pd.DataFrame(index=X.index)
        for col in self.dict_columns:
            processed_json = X[col].apply(jsonstr2dict)
            dict_vals = [
                x if pd.notnull(x) else dict()
                for x in processed_json
            ]
            df_undict_cols = pd.DataFrame.from_dict(dict_vals)
            df_undict_cols = df_undict_cols.add_prefix(col + '_')
            df_undict_cols = df_undict_cols.dropna(axis=1, how='all')
            deeply_undicted_cols = recursive_dict_to_columns(df_undict_cols)
            deeply_undicted_cols.index = result.index
            result = pd.concat([result, deeply_undicted_cols], axis=1)
        return result
    
    
class ColumnFiller(TransformerMixin):
    """
    Służy do uzupełnienia brakujących kolumn po transformacjach rozwijających listy i słowniki.
    """
    def __init__(self):
        self.training_columns = None
        
    def fit(self, X, y=None, **kwargs):
        self.training_columns = X.columns
        return self
        
    def transform(self, X, y=None, **kwargs):
        X = X.copy()
        missing_columns = np.setdiff1d(self.training_columns, X.columns)
        for col in missing_columns:
            X[col] = np.nan
        return X


class TwoColumnsEquals(TransformerMixin):
    """
    Dla podanej listy par kolumn zwraca kolumny wskazujące gdzie podane pary kolumn miały takie same wartości.
    """
    def __init__(self, column_pairs):
        self.column_pairs = column_pairs
    
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        result = pd.DataFrame(index=X.index)
        for column_A, column_B in self.column_pairs:
            new_column_name = (        
                '{}_equals_{}'.format(column_A,column_B)
            )
            result[new_column_name] = (X[column_A] == X[column_B])
        return result


def as_list(x):
    """
    Funkcja pomocnicza do ListUnpackera.
    """
    try:
        return ast.literal_eval(x)
    except:
        return x


def is_null_list(x):
    """
    Funkcja sprawdza czy dana wartość jest brakiem danych. Jest potrzebna dlatego,
    że pd.isnull wywołany na liście/tuplu zwraca wektor, a nie pojedynczą wartość.
    """
    if isinstance(x, (list, tuple)):
        return False
    else:
        return pd.isnull(x)


class ListUnpacker(TransformerMixin):
    """
    Transformacja zamienia kolumny z listami na ramkę z kolumnami zawierającymi kolejne wartości z list.
    Działa tylko na kolumnach, w których listy mają te same długości.
    """
    def __init__(self):
        self.list_columns = None
        
    def fit(self, X, y=None, **kwargs):
        list_columns = []
        for col in X.columns:
            col_with_list_transformed = map(as_list, X[col])
            types = set(map(type, col_with_list_transformed))
            if list not in types:
                continue
            lens = set(map(lambda x: len(x) if isinstance(x, (list, tuple)) else -1, X[col]))
            lens.remove(-1)
            if len(lens) == 1:
                list_columns.append(col)
        self.list_columns = list_columns
        return self
    
    def transform_one_column(self, X, col_name):
        transformed_data_frame = X[col_name].apply(
            lambda x: pd.Series(x) 
            if not is_null_list(x) 
            else pd.Series()
        )
        transformed_data_frame.columns = ["_".join((col_name, str(col))) for col in transformed_data_frame.columns]
        return transformed_data_frame
    
    def transform(self, X, y=None, **kwargs):
        df_list = [self.transform_one_column(X, col) for col in self.list_columns]
        df_list.append(X)
        result_frame = pd.concat(df_list, axis=1)
        result_frame = result_frame.drop(self.list_columns, axis=1)
        return result_frame


class DummyTransformer(TransformerMixin):
    """
    Pusta transformacja. Przydaje się w DaftFeatureUnion gdy chcemy przekazać część Pipelinu bez zmian dalej
    """
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        return X


class ListLengthTransformer(TransformerMixin):
    """
    Zwraca długość każdego elementu w ramce.
    """
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        return X.applymap(lambda x: len(x) if not is_null_list(x) else 0)


class ExtractRegex(TransformerMixin):
    """
    Wyciąga listę wyrażeń pasujących do podanego wyrażenia regularnego dla każdej wartości w ramce wejściowej.
    """
    def __init__(self, regex, first_only=False, regex_name='', **kwargs):
        self.regex = regex
        self.first_only = first_only
        self.regex_name = regex_name

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, **kwargs):
        regex = re.compile(self.regex)
        X_to_return = X.applymap(
            lambda x: self._get_matches(x, regex)
            if not np.all(pd.isnull(x)) else np.nan
        )
        if self.regex_name != '':
            X_to_return.columns = ["_".join((x, "extract_regex(%s)" % self.regex_name)) for x in X.columns]
        return X_to_return

    def _get_matches(self, x, regex):
        results = regex.findall(x)
        if self.first_only:
            if len(results) > 0:
                return results[0]
            return np.nan
        else:
            return results


class AddColumnSuffix(TransformerMixin):
    """
    Dopisuje przyrostek do nazwy kolumny. Przydatne, aby kolumny wychodzące z różnych pipelinów miały różne nazwy.
    """
    def __init__(self, suffix):
        self.suffix = str(suffix)
        
    def fit(self, X, y=None, **kwargs):
        return self
    
    def transform(self, X, y=None, **kwargs):
        X_to_return = X.copy()
        try:
            X_to_return.columns = ["_".join((str(x), self.suffix)) for x in X.columns]
        except:
            import pdb; pdb.set_trace()
        return X_to_return


class PairwiseColumnOperations(TransformerMixin):
    """
    Wykonuje funkcję fn na wszystkich parach kolumn podanych przy inicjalizacji.
    """
    def __init__(self, fn, fn_name, colname_pair_list=None):
        self.fn = fn
        self.fn_name = '__' + fn_name + '__'
        self.colname_pair_list = colname_pair_list

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, **kwargs):
        X_new = pd.DataFrame(index=X.index)
        for col_A, col_B in self.colname_pair_list:
            resulting_colname = self.fn_name + '(' + col_A + ',' + col_B + ')'
            X_new[resulting_colname] = X[[col_A, col_B]].apply(self.fn, axis=1)
        return X_new


def set_similarity_pairwise(p):
    x1, x2 = p
    if is_null_list(x1) or is_null_list(x2):
        return np.nan
    x1 = set(x1)
    x2 = set(x2)
    x12 = x1.intersection(x2)
    if len(x1)>0 or len(x2)>0:
        return 2.0 * len(x12) / (len(x1) + len(x2))
    else:
        return 0.0


def set_intersection(x1, x2):
    x1 = set(x1)
    x2 = set(x2)
    x12 = x1.intersection(x2)
    if len(x1)>0 or len(x2)>0:
        return 2.0 * len(x12) / (len(x1) + len(x2))
    else:
        return 0.0


class ListBinarizer(TransformerMixin):
    """
    Koduje binarnie listy w kolumnach wejściowej ramki. Jeśli ramka zawiera dwie obserwacje, jedna z listą
    ['kot', 'pies'] a druga z ['rybka', 'pies'], to wynikowa ramka będzie wyglądała następująco:

    x_kot|x_pies|x_rybka
      1  |  1   |  0
      0  |  1   |  1

    """
    def __init__(self):
        self.values = {}
        
    def fit(self, X, y=None, **kwargs):
        for col in X.columns:
            col_values = set()
            X[col].apply(lambda x: col_values.update(x) if not is_null_list(x) else x)
            self.values[col] = col_values
        return self
    
    def transform(self, X, y=None, **kwargs):
        data_frames = []
        for col in X.columns:
            new_df = X[col].apply(lambda x: pd.Series([val in x for val in self.values[col]]) if not is_null_list(x) else pd.Series())
            new_df.columns = [col + "_list_binarization_" + val for val in self.values[col]]
            data_frames.append(new_df)
        return pd.concat(data_frames, axis=1)


#### Wasze ficzery

class DictVolume(TransformerMixin):
    def __init__(self, columns):
        self.columns = columns
        # Napisz klasę, która dla zadanej listy nazw kolum słownikowych zwróci
        # rozszerzoną ramkę  danych zawierającą liczbę liści danego słownika.

    def flatten(self,dict_):
        count = 0
        if type(dict_) not in (str, dict):
            return 0
        if type(dict_) == str:
            dict_ = json.loads(dict_)
        if type(dict_) != dict:
            return 0
        for value in dict_.values():
            if type(value) == dict:
                count += self.flatten(value)
            else:
                count += 1
        return count

    def fit(self,X): # jak niżej
        return self

    def transform(self, X): # jak niżej
        result = pd.DataFrame(index=X.index)
        for col in self.columns:
            result[col] = X[col].apply(self.flatten)
        return result


class NumberOfLanguages(TransformerMixin):
    def __init__(self, columns, pattern = r'[^-][a-z][a-z]'):
        self.columns = columns
        self.pattern = re.compile(pattern)

    def count(self, string):
        count = 0
        string = str(string)
        string = " " + string
        languages = self.pattern.findall(string)
        languages = [language[1:] for language in languages]
        return len(set(languages))

    def fit(self,X): # dopisać y 
        return self

    def transform(self, X): # dopisać y 
        result = pd.DataFrame(index=X.index)
        for col in self.columns:
            result[col] = X[col].apply(self.count)
        return result

import operator

class ColumnsRelation(TransformerMixin):
    def __init__(self,columns,operator,mode='num'):
        self.columns = columns
        self.operator = operator
        self.mode = mode

    def apply_oper(self,c1, relate, c2):
        ops = {'=': operator.eq,
        '>': operator.gt,
        '<': operator.lt,
        '>=': operator.ge,
        '<=': operator.le,
        '-': operator.sub,
        '+': operator.add,
        '/': operator.truediv,
        '*': operator.mul}
        return ops[relate](c1, c2)

    def fit(self, X, y=None, **kwargs):
        return self

    def transform(self, X, y=None, **kwargs):
        new_X = pd.DataFrame()
        c1 = X[self.columns[0]].fillna('')
        c2 = X[self.columns[1]].fillna('')
        if self.mode!='cat':
            c1 = pd.to_numeric(c1, errors='coerce').fillna(0)
            c2 = pd.to_numeric(c2, errors='coerce').fillna(0)
        new_X[self.columns[0]+'_'+self.operator+'_'+self.columns[1]] = (self.apply_oper(c1,self.operator,c2)).fillna(0).replace(np.inf,0)
        new_X.set_index(X.index,inplace=True)
        return new_X
